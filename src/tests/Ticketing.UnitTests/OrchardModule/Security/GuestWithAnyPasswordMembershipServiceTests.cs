﻿namespace Ticketing.UnitTests.OrchardModule.Security
{
    using NSubstitute;

    using NUnit.Framework;

    using Orchard.ContentManagement;
    using Orchard.Security;

    using Ticketing.OrchardModule.Services.Security;

    [TestFixture]
    public class GuestWithAnyPasswordMembershipServiceTests
    {
        [Test]
        public void ValidateUser_when_user_is_guest_and_is_logged_for_the_first_time_no_password_is_checked_and_user_is_created()
        {
            // Arrange
            var createdUser = new FakeUser();
            var internalService = Substitute.For<IMembershipService>();
            internalService.GetUser("guest").Returns((IUser)null);
            internalService.CreateUser(null).ReturnsForAnyArgs(createdUser);

            var sut = GuestWithAnyPasswordMembershipService.CreateAsDecoratorFor(internalService);

            // Act
            var validatedUser = sut.ValidateUser("guest", "any password");

            // Assert
            internalService.ReceivedWithAnyArgs(1).CreateUser(null);
            Assert.That(validatedUser, Is.EqualTo(createdUser));
        }

        [Test]
        public void ValidateUser_when_user_is_guest_and_has_account_in_internal_service_then_no_password_is_checked_and_user_from_internal_system_is_returned()
        {
            // Arrange
            var existingUser = new FakeUser();
            var internalService = Substitute.For<IMembershipService>();
            internalService.GetUser("guest").Returns(existingUser);
            internalService.CreateUser(null).ReturnsForAnyArgs(existingUser);

            var sut = GuestWithAnyPasswordMembershipService.CreateAsDecoratorFor(internalService);

            // Act
            var validatedUser = sut.ValidateUser("guest", "any password");

            // Assert
            internalService.ReceivedWithAnyArgs(0).CreateUser(null);
            Assert.That(validatedUser, Is.EqualTo(existingUser));
        }

        [Test]
        public void ValidateUser_when_user_is_not_guest_logic_is_delegated_to_internal_service()
        {
            // Arrange
            var internalService = Substitute.For<IMembershipService>();

            var sut = GuestWithAnyPasswordMembershipService.CreateAsDecoratorFor(internalService);

            // Act
            var validatedUser = sut.ValidateUser("notGuest", "any password");

            // Assert
            internalService.ReceivedWithAnyArgs(1).ValidateUser("notGuest", "any password");
            Assert.That(validatedUser, Is.Not.Null);
        }

        private class FakeUser : IUser
        {
            public ContentItem ContentItem { get; private set; }

            public int Id { get; private set; }

            public string UserName { get; private set; }

            public string Email { get; private set; }
        }
    }
}
