﻿namespace Ticketing.UnitTests.OrchardModule.Models
{
    using NUnit.Framework;

    using Ticketing.OrchardModule.Models;

    [TestFixture]
    public class SettingsPartTests
    {
        [Test]
        public void Data_access_type_can_be_persisited_in_record()
        {
            // Arrange
            var sut = new SettingsPart();
            sut.Record = new SettingsPartRecord();

            // Act
            sut.DataAccessType = DataAccessType.NHibernate;

            // Assert
            Assert.That(sut.DataAccessType, Is.EqualTo(DataAccessType.NHibernate));
        }

        [Test]
        public void Data_access_type_should_return_InMemory_if_data_record_has_empty_value()
        {
            // Arrange
            var sut = new SettingsPart();
            sut.Record = new SettingsPartRecord();

            // Act
            sut.Record.DataAccessType = null;

            // Assert
            Assert.That(sut.DataAccessType, Is.EqualTo(DataAccessType.InMemory));
        }
    }
}