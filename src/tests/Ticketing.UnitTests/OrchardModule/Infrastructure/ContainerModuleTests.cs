﻿namespace Ticketing.UnitTests.OrchardModule.Infrastructure
{
    using System;

    using Autofac;

    using NSubstitute;

    using NUnit.Framework;

    using Orchard;
    using Orchard.ContentManagement;
    using Orchard.Settings;

    using Ticketing.Common;
    using Ticketing.OrchardModule.Infrastructure;
    using Ticketing.OrchardModule.Models;
    using Ticketing.TicketManagement.CommandHandlers;
    using Ticketing.TicketManagement.Commands;
    using Ticketing.TicketManagement.Queries;

    [TestFixture]
    public class ContainerModuleTests
    {
        private IContainer Container { get; set; }

        [SetUp]
        public void SetUp()
        {
            var builder = new ContainerBuilder();

            var orchardServices = Substitute.For<IOrchardServices>();
            var settings = new SettingsPart();
            settings.Record = new SettingsPartRecord
            {
                DataAccessType = "NHibernate",
                ConnectionString = "another connection string"
            };
            var contentItem = new ContentItem();
            contentItem.Weld(settings);
            orchardServices.WorkContext.Returns(Substitute.For<WorkContext>());
            orchardServices.WorkContext.CurrentSite.Returns(Substitute.For<ISite>());
            orchardServices.WorkContext.CurrentSite.ContentItem.Returns(call => contentItem);
            builder.RegisterInstance(orchardServices);

            builder.RegisterModule(new ContainerModule());

            this.Container = builder.Build();
        }

        [TearDown]
        public void TearDown()
        {
            this.Container.Dispose();
        }

        [Test]
        [TestCase(typeof(ICommandHandler<CreateNewProjectCommand>), typeof(ProjectService))]
        [TestCase(typeof(IProjectAdminListQuery), typeof(ProjectAdminListQuery))]
        public void Can_be_resolved_as_single(Type toResolve, Type expected)
        {
            // Act
            var resolved = this.Container.Resolve(toResolve);

            // Assert
            Assert.That(resolved.GetType(), Is.EqualTo(expected));
        }

/*
        [Test]
        [TestCase(typeof(IEventHandler<CaseCreated>), typeof(CaseListViewProjection))]
        public void Can_be_resolved_as_one_of(Type toResolve, Type expected)
        {
            // Act
            var enumerableTypes = typeof(IEnumerable<>).MakeGenericType(toResolve);
            var resolved = this.Container.Resolve(enumerableTypes) as Array;

            // Assert
            Assert.That(resolved, Has.Some.Matches<object>(x => x.GetType() == expected));
        }
*/
    }
}
