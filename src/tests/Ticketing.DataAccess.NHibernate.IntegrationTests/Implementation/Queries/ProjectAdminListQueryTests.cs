﻿namespace Ticketing.DataAccess.NHibernate.IntegrationTests.Implementation.Queries
{
    using NUnit.Framework;

    using Ticketing.DataAccess.NHibernate.Implementation;
    using Ticketing.TicketManagement.Domain;
    using Ticketing.TicketManagement.Queries;

    [TestFixture]
    public class ProjectAdminListQueryTests : NHibernateRollbackTestCase
    {
        private IProjectAdminListQuery sutQuery;

        [SetUp]
        public override void SetUp()
        {
            base.SetUp();
            var repository = new Repository<Project>(this.SessionProvider);
            this.sutQuery = new ProjectAdminListQuery(repository);
        }

        [Test]
        public void Execute_returns_empty_result_for_no_data()
        {
            // Arrange
            
            // Act
            var result = this.sutQuery.Execute();

            // Assert
            Assert.That(result, Is.Not.Null);
            Assert.That(result.PageOfItems, Is.Not.Null);
            Assert.That(result.PageOfItems, Is.Empty);
        }
    }
}