﻿namespace Ticketing.DataAccess.NHibernate.IntegrationTests
{
    using FluentNHibernate.Automapping;
    using FluentNHibernate.Cfg;

    using global::NHibernate;

    using global::NHibernate.Cfg;

    using global::NHibernate.Tool.hbm2ddl;

    using Ticketing.DataAccess.NHibernate.Mappings;

    public class DataUtility
    {
        public static Configuration NHibernateConfiguration { get; set; }

        public static AutoPersistenceModel PersistenceModel { get; set; }

        public static FluentConfiguration FluentConfiguration { get; set; }

        public static ISessionFactory CreateTestSessionFactory()
        {
            NHibernateConfiguration = BuildConfiguration();
            var schemaExport = new SchemaExport(NHibernateConfiguration);
            schemaExport.Drop(false, true);
            schemaExport.Create(false, true);
            return NHibernateConfiguration.BuildSessionFactory();
        }

        public static Configuration BuildConfiguration()
        {
            var persistenceModel = new AutoPersistenceModel();
            var extender = new AutoPersistenceModelExtender(null);
            extender.Extend(persistenceModel);

            var conf = new Configuration();
            conf.Configure();

            return Fluently.Configure(conf)
                    .Mappings(m => m.AutoMappings.Add(persistenceModel))
                    .BuildConfiguration();
        }
    }
}