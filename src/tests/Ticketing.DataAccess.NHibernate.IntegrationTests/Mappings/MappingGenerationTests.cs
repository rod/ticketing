﻿namespace Ticketing.DataAccess.NHibernate.IntegrationTests.Mappings
{
    using global::NHibernate;

    using NUnit.Framework;

    [TestFixture]
    public class MappingGenerationTests : NHibernateRollbackTestCase
    {
        [Test]
        public void CanConfirmDatabaseMatchesMappings()
        {
            var allClassMetadata = this.SessionFactory.GetAllClassMetadata();

            foreach (var entry in allClassMetadata)
            {
                var session = this.SessionFactory.OpenSession();
                session.CreateCriteria(entry.Value.GetMappedClass(EntityMode.Poco))
                        .SetMaxResults(0).List();
            }
        }
    }
}
