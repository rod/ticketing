﻿namespace Ticketing.DataAccess.NHibernate.IntegrationTests.Mappings
{
    using NUnit.Framework;

    using Ticketing.TicketManagement.Domain;

    [TestFixture]
    public class ProjectMappingTests : NHibernateRollbackTestCase
    {
        [Test]
        public void Can_save_new_model()
        {
            // Arrange
            var model = new Project("New Project", "John Doe");

            // Act
            this.Session.Save(model);
            this.Session.Flush();
            this.Session.Clear();

            // Assert
            var modelFromDb = this.Session.Get<Project>(model.Id);
            Assert.AreEqual("New Project", modelFromDb.Name);
            Assert.AreEqual("John Doe", modelFromDb.Owner);
            Assert.AreEqual(model.Status, modelFromDb.Status);
            Assert.AreEqual(model.GetStatusDate().AddMilliseconds(model.GetStatusDate().Millisecond * -1).ToString(), modelFromDb.GetStatusDate().ToString());
        }
    }
}