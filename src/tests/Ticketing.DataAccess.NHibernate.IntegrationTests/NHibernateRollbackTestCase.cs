﻿namespace Ticketing.DataAccess.NHibernate.IntegrationTests
{
    using System;

    using global::NHibernate;

    using NUnit.Framework;

    [TestFixture]
    public class NHibernateRollbackTestCase
    {
        static NHibernateRollbackTestCase()
        {
            StaticSessionFactory = DataUtility.CreateTestSessionFactory();

            // Start Log4Net
            log4net.Config.XmlConfigurator.Configure();
        }

        public static ISessionFactory StaticSessionFactory { get; set; }

        protected bool ToCommit { get; set; }

        protected ISessionFactory SessionFactory
        {
            get
            {
                return StaticSessionFactory;
            }
        }

        protected ISession Session { get; set; }

        protected IStatelessSession StatelessSession { get; set; }

        protected ISessionProvider SessionProvider { get; set; }

        protected ISessionFactoryProvider SessionFactoryProvider { get; set; }

        protected ITransaction Transaction { get; set; }

        protected ITransaction TransactionStateless { get; set; }

        [SetUp]
        public virtual void SetUp()
        {
            this.Session = this.SessionFactory.OpenSession();
            this.StatelessSession = this.SessionFactory.OpenStatelessSession();
            this.Transaction = this.Session.BeginTransaction();
            this.TransactionStateless = this.StatelessSession.BeginTransaction();
            this.SessionProvider = new TestSessionProvider(this.Session, this.StatelessSession);
            this.SessionFactoryProvider = new TestSessionFactoryProvider(this.SessionFactory);
            this.ToCommit = false;
        }

        [TearDown]
        public virtual void TearDown()
        {
            if (this.TransactionStateless.IsActive)
            {
                if (this.ToCommit)
                {
                    this.TransactionStateless.Commit();
                }
                else
                {
                    this.TransactionStateless.Rollback();
                }
            }

            if (this.Transaction.IsActive)
            {
                if (this.ToCommit)
                {
                    this.Transaction.Commit();
                }
                else
                {
                    this.Transaction.Rollback();
                }
            }
        }

        public class TestSessionProvider : ISessionProvider
        {
            private readonly ISession session;

            private readonly IStatelessSession statelessSession;

            public TestSessionProvider(ISession session, IStatelessSession statelessSession)
            {
                this.session = session;
                this.statelessSession = statelessSession;
            }

            public ISession GetSession()
            {
                return this.session;
            }

            public IStatelessSession GetStatelessSession()
            {
                return this.statelessSession;
            }

            public void CloseSessionWithoutDisposing()
            {
                throw new NotImplementedException();
            }
        }

        public class TestSessionFactoryProvider : ISessionFactoryProvider
        {
            private readonly ISessionFactory sessionFactory;

            public TestSessionFactoryProvider(ISessionFactory sessionFactory)
            {
                this.sessionFactory = sessionFactory;
            }

            public ISessionFactory GetFactory()
            {
                return this.sessionFactory;
            }
        }
    }
}