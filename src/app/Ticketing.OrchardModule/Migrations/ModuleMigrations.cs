﻿namespace Ticketing.OrchardModule.Migrations
{
    using Orchard.Data.Migration;

    public class ModuleMigrations : DataMigrationImpl
    {
        public int Create()
        {
            // Creating table SettingsPartRecord
            this.SchemaBuilder.CreateTable(
                "SettingsPartRecord",
                table => table
                    .ContentPartRecord()
                    .Column<string>("DataAccessType", co => co.WithLength(25))
                    .Column<string>("ConnectionString", co => co.WithLength(255)));
            return 1;
        }

        public int UpdateFrom1()
        {
            // SOMETHING
            return 2;
        }
    }
}