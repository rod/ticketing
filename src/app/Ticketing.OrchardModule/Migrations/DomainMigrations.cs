﻿namespace Ticketing.OrchardModule.Migrations
{
    using System;
    using System.Data;

    using Orchard.Data.Migration;
    using Orchard.Data.Migration.Schema;

    using Ticketing.OrchardModule.Infrastructure.Data;

    public class DomainMigrations : DataMigrationImpl
    {
        public override SchemaBuilder SchemaBuilder
        {
            get
            {
                return base.SchemaBuilder;
            }

            set
            {
                base.SchemaBuilder = new SchemaBuilder(
                    value.Interpreter, DataConfigurationEventHandler.DomainTablePrefix, value.FormatPrefix);
            }
        }

        public int Create()
        {
            this.SchemaBuilder.CreateTable(
                "Project",
                table => table
                    .Column("Id", DbType.Guid, co => co.PrimaryKey())
                    .Column<string>("Name", co => co.NotNull().WithLength(50))
                    .Column<string>("Owner", co => co.Nullable().WithLength(50))
                    .Column<string>("Status", co => co.NotNull().WithLength(10))
                    .Column<DateTime>("StartingDate", co => co.NotNull())
                    .Column<DateTime>("FinishedDate", co => co.Nullable()));
            return 1;
        }
    }
}