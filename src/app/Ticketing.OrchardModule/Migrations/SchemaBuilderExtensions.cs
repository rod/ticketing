﻿namespace Ticketing.OrchardModule.Migrations
{
    using System.Reflection;

    using Orchard.Data.Migration.Schema;

    public static class SchemaBuilderExtensions
    {
        public static void SetFeaturePrefix(this SchemaBuilder builder, string prefix)
        {
            if (builder == null)
            {
                return;
            }

            var type = typeof(SchemaBuilder);
            var featureField = type.GetField(
                "_featurePrefix", BindingFlags.Instance & BindingFlags.NonPublic);
            featureField.SetValue(builder, prefix);
        }
    }
}