﻿namespace Ticketing.OrchardModule.Drivers
{
    using Orchard.ContentManagement.Drivers;
    using Orchard.Localization;
    using Orchard.UI.Notify;

    using Ticketing.OrchardModule.Models;

    public class SettingsPartDriver : ContentPartDriver<SettingsPart>
    {
        private const string TemplateName = "Parts/Settings";

        private readonly INotifier notifier;

        public SettingsPartDriver(INotifier notifier)
        {
            this.notifier = notifier;
            this.T = NullLocalizer.Instance;
        }

        public Localizer T { get; set; }

        protected override DriverResult Editor(SettingsPart part, dynamic shapeHelper)
        {
            return this.ContentShape(
                "Parts_Settings_Edit",
                () => shapeHelper.EditorTemplate(TemplateName: TemplateName, Model: part, Prefix: this.Prefix))
                .OnGroup("Ticketing");
        }

        protected override DriverResult Editor(
            SettingsPart part, Orchard.ContentManagement.IUpdateModel updater, dynamic shapeHelper)
        {
            if (updater.TryUpdateModel(part, this.Prefix, null, null))
            {
                this.notifier.Information(this.T("Content ticketing settings updated successfully."));
            }
            else
            {
                this.notifier.Error(this.T("Error during content ticketing settings update!"));
            }

            return this.Editor(part, shapeHelper);
        }
    }
}