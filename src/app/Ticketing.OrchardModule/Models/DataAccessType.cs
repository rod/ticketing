namespace Ticketing.OrchardModule.Models
{
    public enum DataAccessType
    {
        /// <summary>
        /// Uses memory to store data.
        /// </summary>
        InMemory,

        /// <summary>
        /// Uses NHibernate to store data.
        /// </summary>
        NHibernate,
    }
}