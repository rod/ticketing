﻿namespace Ticketing.OrchardModule.Models
{
    using System.ComponentModel.DataAnnotations;

    using Orchard.ContentManagement.Records;

    public class SettingsPartRecord : ContentPartRecord
    {
        public virtual string DataAccessType { get; set; }

        public virtual string ConnectionString { get; set; }
    }
}