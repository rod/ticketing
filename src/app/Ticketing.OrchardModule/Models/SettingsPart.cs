﻿namespace Ticketing.OrchardModule.Models
{
    using System;

    using Orchard.ContentManagement;

    using Ticketing.OrchardModule.Infrastructure;

    public class SettingsPart : ContentPart<SettingsPartRecord>
    {
        public DataAccessType DataAccessType
        {
            get
            {
                var result = DataAccessType.InMemory;
                Enum.TryParse(this.Record.DataAccessType, false, out result);
                return result;
            }

            set
            {
                this.Record.DataAccessType = value.ToString();
            }
        }

        public string ConnectionString
        {
            get
            {
                return this.Record.ConnectionString;
            }

            set
            {
                this.Record.ConnectionString = value;
            }
        }
    }
}