﻿namespace Ticketing.OrchardModule
{
    using System.Collections.Generic;

    using Orchard.Environment.Extensions.Models;
    using Orchard.Security.Permissions;

    public class Permissions : IPermissionProvider
    {
        public static readonly Permission ManageProjects = new Permission
                                                               {
                                                                   Description = "Managing projects",
                                                                   Name = "ManageProjects"
                                                               };

        public static readonly Permission ReadProject = new Permission
                                                            {
                                                                Description = "Reading projects",
                                                                Name = "ReadingProjects",
                                                            };

        private static readonly IEnumerable<PermissionStereotype> DefaultStereotypes = new[]
                                                                                           {
                                                                                               new PermissionStereotype
                                                                                               {
                                                                                                     Name = "Administrator",
                                                                                                     Permissions = new[]
                                                                                                     {
                                                                                                         ManageProjects,
                                                                                                     },
                                                                                               },
                                                                                               new PermissionStereotype
                                                                                               {
                                                                                                   Name = "Ticketing - Project Manager",
                                                                                                   Permissions = new[]
                                                                                                   {
                                                                                                       ManageProjects,
                                                                                                   },
                                                                                               },
                                                                                           };

        private static readonly IEnumerable<Permission> PossiblePermissions = new[]
        {
            ManageProjects,
            ReadProject,
        };

        public Feature Feature { get; set; }

        public IEnumerable<Permission> GetPermissions()
        {
            return PossiblePermissions;
        }

        public IEnumerable<PermissionStereotype> GetDefaultStereotypes()
        {
            return DefaultStereotypes;
        }
    }
}