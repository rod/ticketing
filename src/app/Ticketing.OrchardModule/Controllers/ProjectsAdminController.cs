﻿namespace Ticketing.OrchardModule.Controllers
{
    using System;
    using System.Linq;
    using System.Web.Mvc;

    using Orchard;
    using Orchard.Core.Contents.Controllers;
    using Orchard.Localization;
    using Orchard.UI.Admin;
    using Orchard.UI.Notify;

    using Ticketing.Common.Data;
    using Ticketing.Common.Infrastructure;
    using Ticketing.TicketManagement.Commands;
    using Ticketing.TicketManagement.Domain;
    using Ticketing.TicketManagement.Queries;

    public enum ProjectAdminIndexBulkActions
    {
        /// <summary>
        /// None action
        /// </summary>
        None,

        /// <summary>
        /// Project termination
        /// </summary>
        Terminate,
    }

    /// <summary>
    /// The projects admin controller.
    /// </summary>
    [Admin]
    public partial class ProjectsAdminController : Controller
    {
        private readonly ICommandSender commandSender;

        private readonly IQueryFactory queryFactory;

        private readonly IOrchardServices services;

        public ProjectsAdminController(IOrchardServices services, ICommandSender commandSender, IQueryFactory queryFactory)
        {
            this.services = services;
            this.commandSender = commandSender;
            this.queryFactory = queryFactory;
            this.T = NullLocalizer.Instance;
        }

        public Localizer T { get; set; }

        public ActionResult CreateNew()
        {
            var formModel = new CreateFormModel();
            return this.View(formModel);
        }

        [HttpPost]
        [ActionName("CreateNew")]
        public ActionResult CreateNewPost([Bind(Prefix = "Command", Exclude = "CreatedProjectId")]CreateNewProjectCommand command)
        {
            if (ModelState.IsValid == false)
            {
                return this.View(CreateCreateFormModel(command));
            }

            this.commandSender.Send(command);
            if (command.CreatedProjectId != null)
            {
                this.services.Notifier.Information(
                    this.T("Project created successfully with id {0}", command.CreatedProjectId.Value));
            }
            else
            {
                this.services.Notifier.Error(this.T("Project creation failed."));
                return this.View(CreateCreateFormModel(command));
            }

            return this.RedirectToAction("Index");
        }

        public ActionResult Index()
        {
            var viewModel = new IndexViewModel();
            var query = this.queryFactory.Create<IProjectAdminListQuery>();
            var statusString = this.Request["Status"];
            ProjectStatuses status;
            if (Enum.TryParse(statusString, out status))
            {
                query.Status = status;
            }

            viewModel.QueryResult = query.Execute();
            return this.View(viewModel);
        }

        [HttpPost]
        [ActionName("Index")]
        [FormValueRequired("submit.BulkAction")]
        public ActionResult IndexBulkActionPost(Guid[] ids, ProjectAdminIndexBulkActions bulkAction)
        {
            switch (bulkAction)
            {
                case ProjectAdminIndexBulkActions.None:
                    break;
                case ProjectAdminIndexBulkActions.Terminate:
                    var command = new TerminateProjectsCommand { ProjectIdsToTerminate = ids };
                    this.commandSender.Send(command);
                    this.services.Notifier.Information(this.T("Projects {0} has been terminated", string.Join(", ", command.ProjectIdsTerminated.Select(x => x.ToString()))));
                    break;
                default:
                    throw new ArgumentOutOfRangeException("bulkAction");
            }

            return this.RedirectToAction("Index");
        }

        [HttpPost]
        [ActionName("Index")]
        [FormValueRequired("submit.OtherAction")]
        public ActionResult IndexOtherActionPost(Guid[] ids, ProjectAdminIndexBulkActions bulkAction)
        {
            return null;
        }

        protected override void OnAuthorization(AuthorizationContext filterContext)
        {
            if (this.services.Authorizer.Authorize(Permissions.ManageProjects) == false)
            {
                filterContext.Result = new HttpUnauthorizedResult();
            }

            base.OnAuthorization(filterContext);
        }

        private static CreateFormModel CreateCreateFormModel(CreateNewProjectCommand command)
        {
            var formModel = new CreateFormModel();
            formModel.Command = command;
            return formModel;
        }
    }
}