﻿namespace Ticketing.OrchardModule.Controllers
{
    using System.Web.Mvc;

    using Orchard.Themes;

    [Themed]
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return this.View();
        }

        public PartialViewResult AjaxReturn()
        {
            return null;
        }
    }
}
