﻿namespace Ticketing.OrchardModule.Controllers
{
    using Ticketing.Common.Data;
    using Ticketing.TicketManagement.Commands;
    using Ticketing.TicketManagement.Queries.ResultItems;

    /// <summary>
    /// The projects admin controller.
    /// </summary>
    public partial class ProjectsAdminController
    {
        public class CreateFormModel
        {
            public CreateFormModel()
            {
                this.Command = new CreateNewProjectCommand();
            }

            public CreateNewProjectCommand Command { get; set; }
        }

        public class IndexViewModel
        {
            public PagedResult<ProjectAdminListItem> QueryResult { get; set; }
        }
    }
}
