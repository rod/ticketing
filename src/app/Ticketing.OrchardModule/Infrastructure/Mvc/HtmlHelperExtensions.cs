namespace Ticketing.OrchardModule.Infrastructure.Mvc
{
    using System;
    using System.Collections.Generic;
    using System.Linq.Expressions;
    using System.Web.Mvc;
    using System.Web.Mvc.Html;

    public static class HtmlHelperExtensions
    {
        public static MvcHtmlString DropDownListForEnum<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TProperty>> expression, Type enumType)
        {
            var list = new List<SelectListItem>();
            var values = Enum.GetValues(enumType);
            foreach (var item in values)
            {
                list.Add(new SelectListItem() { Value = item.ToString(), Text = item.ToString() });
            }

            return htmlHelper.DropDownListFor(expression, list);
        }
    }
}