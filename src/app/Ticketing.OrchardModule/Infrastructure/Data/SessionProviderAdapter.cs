﻿namespace Ticketing.OrchardModule.Infrastructure.Data
{
    using System;

    using NHibernate;

    using Orchard.Data;

    using Ticketing.DataAccess.NHibernate;

    public class SessionProviderAdapter : ISessionProvider
    {
        private readonly ISessionLocator sessionLocator;

        public SessionProviderAdapter(ISessionLocator sessionLocator)
        {
            this.sessionLocator = sessionLocator;
        }

        public ISession GetSession()
        {
            return this.sessionLocator.For(typeof(object));
        }

        public IStatelessSession GetStatelessSession()
        {
            throw new NotImplementedException();
        }
    }
}