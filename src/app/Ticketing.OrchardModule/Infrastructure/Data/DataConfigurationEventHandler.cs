﻿namespace Ticketing.OrchardModule.Infrastructure.Data
{
    using System.Diagnostics;
    using System.Reflection;

    using FluentNHibernate.Automapping;
    using FluentNHibernate.Cfg;

    using NHibernate.Cfg;

    using Orchard.Data;
    using Orchard.Environment.Configuration;
    using Orchard.Logging;
    using Orchard.Utility;

    using Ticketing.DataAccess.NHibernate.Mappings;

    public class DataConfigurationEventHandler : ISessionConfigurationEvents
    {
        internal const string DomainTablePrefix = "TicketingDomain";

        private ShellSettings shellSettings;

        public DataConfigurationEventHandler(ShellSettings shellSettings)
        {
            this.shellSettings = shellSettings;
            this.Logger = NullLogger.Instance;
        }

        public ILogger Logger { get; set; }

        public void Created(FluentConfiguration cfg, AutoPersistenceModel defaultModel)
        {
            var prefix = string.IsNullOrWhiteSpace(this.shellSettings.DataTablePrefix) == false
                             ? this.shellSettings.DataTablePrefix
                             : string.Empty;
            prefix = string.Concat(prefix, DomainTablePrefix, "_");

            var extender = new AutoPersistenceModelExtender(prefix);
            extender.Extend(defaultModel);
        }

        public void Prepared(FluentConfiguration cfg)
        {
        }

        public void Building(Configuration cfg)
        {
        }

        public void Finished(Configuration cfg)
        {
        }

        public void ComputingHash(Hash hash)
        {
#if DEBUG
            var timer = Stopwatch.StartNew();
#endif
            var typeSource = new TypeSource();

            foreach (var recordType in typeSource.GetTypes())
            {
                hash.AddTypeReference(recordType);

                if (recordType.BaseType != null)
                {
                    hash.AddTypeReference(recordType.BaseType);
                }

                foreach (var property in recordType.GetProperties(BindingFlags.DeclaredOnly | BindingFlags.Public))
                {
                    hash.AddString(property.Name);
                    hash.AddTypeReference(property.PropertyType);

                    foreach (var attr in property.GetCustomAttributesData())
                    {
                        hash.AddTypeReference(attr.Constructor.DeclaringType);
                    }
                }
            }
#if DEBUG
            timer.Stop();

            this.Logger.Information("ComputingHash time: {0} ms", timer.ElapsedMilliseconds);
#endif
        }
    }
}