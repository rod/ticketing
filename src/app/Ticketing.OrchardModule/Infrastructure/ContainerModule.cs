namespace Ticketing.OrchardModule.Infrastructure
{
    using System.Reflection;

    using Autofac;

    using Ticketing.Common;
    using Ticketing.Common.Data;
    using Ticketing.DataAccess.NHibernate;
    using Ticketing.DataAccess.NHibernate.Implementation;
    using Ticketing.OrchardModule.Infrastructure.Data;

    using Module = Autofac.Module;

    public class ContainerModule : Module
    {
        internal static void LoadCommonDependenciesForPersistence(ContainerBuilder builder)
        {
        }

        protected override void Load(ContainerBuilder builder)
        {
            var coreAssembly = Assembly.Load("Ticketing.Core");
            var dataAccessAssembly = Assembly.Load("Ticketing.DataAccess");

            builder.RegisterType<ContainerBus>().AsImplementedInterfaces().InstancePerDependency();
            builder.RegisterType<ContainerQueryFactory>().AsImplementedInterfaces().InstancePerDependency();

            builder.RegisterAssemblyTypes(coreAssembly)
                   .AsClosedTypesOf(typeof(ICommandHandler<>))
                   .InstancePerDependency();

            builder.RegisterAssemblyTypes(coreAssembly)
                   .AsImplementedInterfaces()
                   .Where(x => x.IsClosedTypeOf(typeof(IQuery<>)))
                   .InstancePerDependency();
            builder.RegisterAssemblyTypes(dataAccessAssembly)
                   .AsImplementedInterfaces()
                   .Where(x => x.IsClosedTypeOf(typeof(IQuery<>)))
                   .InstancePerDependency();

            builder.RegisterType<SessionProviderAdapter>()
                   .As<ISessionProvider>()
                   .InstancePerLifetimeScope();

            builder.RegisterGeneric(typeof(Repository<>))
                   .As(typeof(IRepository<>))
                   .InstancePerLifetimeScope();
        }
    }
}