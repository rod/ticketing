﻿namespace Ticketing.OrchardModule.Infrastructure
{
    using System;
    using System.Collections.Generic;

    using Autofac;

    using Orchard.Logging;

    using Ticketing.Common;
    using Ticketing.Common.Infrastructure;

    public class ContainerBus : ICommandSender
    {
        private readonly IComponentContext componentContext;

        public ContainerBus(IComponentContext componentContext)
        {
            this.componentContext = componentContext;
            this.Logger = NullLogger.Instance;
        }

        public ILogger Logger { get; set; }

        public void Send<T>(T command) where T : ICommand
        {
            var commandHandlers = new List<ICommandHandler<T>>(this.componentContext.Resolve<IEnumerable<ICommandHandler<T>>>());
            var count = commandHandlers.Count;
            if (count != 1)
            {
                throw new InvalidOperationException("cannot send to more than one handler");
            }

            if (count == 0)
            {
                throw new InvalidOperationException("no handler registered");
            }

            commandHandlers[0].HandleCommand(command);
        }
    }
}