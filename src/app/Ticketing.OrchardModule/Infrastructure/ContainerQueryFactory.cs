﻿namespace Ticketing.OrchardModule.Infrastructure
{
    using Autofac;

    using Orchard.Logging;

    using Ticketing.Common.Data;

    public class ContainerQueryFactory : IQueryFactory
    {
        private readonly IComponentContext componentContext;

        public ContainerQueryFactory(IComponentContext componentContext)
        {
            this.componentContext = componentContext;
            this.Logger = NullLogger.Instance;
        }

        public ILogger Logger { get; set; }

        public T Create<T>()
        {
            return this.componentContext.Resolve<T>();
        }
    }
}