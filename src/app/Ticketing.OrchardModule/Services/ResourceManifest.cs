﻿namespace Ticketing.OrchardModule.Services
{
    using Orchard.UI.Resources;

    public class ResourceManifest : IResourceManifestProvider
    {
        public void BuildManifests(ResourceManifestBuilder builder)
        {
            var manifest = builder.Add();
            manifest.DefineStyle("ticketing").SetUrl("folder/ticketing.css", "ticketing.css").SetVersion("1.0");
        }
    }
}