﻿namespace Ticketing.OrchardModule.Services.Security
{
    using System;
    using System.Collections.Generic;

    using Orchard;
    using Orchard.Environment.Extensions;
    using Orchard.Localization;
    using Orchard.Logging;
    using Orchard.Messaging.Services;
    using Orchard.Security;
    using Orchard.Services;
    using Orchard.Users.Events;
    using Orchard.Users.Services;

    /// <summary>
    /// Proxy class which extends typical Orchard MembershipService from Users module.
    /// If user login is guest then no password is checked.
    /// We can delegate checking of guest user to external system.
    /// This class can be used as decorator for passed class also
    /// </summary>
    [OrchardSuppressDependency("Orchard.Users.Services.MembershipService")]
    public class GuestWithAnyPasswordMembershipService : IMembershipService
    {
        private readonly IMembershipService internalService;

        public GuestWithAnyPasswordMembershipService(
            IOrchardServices orchardServices,
            IMessageManager messageManager,
            IEnumerable<IUserEventHandler> userEventHandlers,
            IClock clock,
            IEncryptionService encryptionService)
        {
            this.Logger = NullLogger.Instance;
            this.T = NullLocalizer.Instance;

            this.internalService = new MembershipService(
                orchardServices,
                messageManager,
                userEventHandlers,
                clock,
                encryptionService);
        }

        private GuestWithAnyPasswordMembershipService(IMembershipService proxiedService)
        {
            this.Logger = NullLogger.Instance;
            this.T = NullLocalizer.Instance;

            // Make myself as proxy;
            this.internalService = proxiedService;
        }

        public ILogger Logger { get; set; }

        public Localizer T { get; set; }

        public static GuestWithAnyPasswordMembershipService CreateAsDecoratorFor(IMembershipService decoratedService)
        {
            return new GuestWithAnyPasswordMembershipService(decoratedService);
        }

        public IUser CreateUser(CreateUserParams createUserParams)
        {
            return this.internalService.CreateUser(createUserParams);
        }

        public IUser GetUser(string username)
        {
            return this.internalService.GetUser(username);
        }

        public IUser ValidateUser(string userNameOrEmail, string password)
        {
            if ("guest".Equals(userNameOrEmail, StringComparison.OrdinalIgnoreCase))
            {
                var orchardUser = this.internalService.GetUser(userNameOrEmail);
                if (orchardUser == null)
                {
                    orchardUser =
                        this.internalService.CreateUser(
                            new CreateUserParams("guest", "anything", "guest@localhost.com", "none", "none", true));
                }

                return orchardUser;
            }

            return this.internalService.ValidateUser(userNameOrEmail, password);
        }

        public void SetPassword(IUser user, string password)
        {
            this.internalService.SetPassword(user, password);
        }

        public MembershipSettings GetSettings()
        {
            return this.internalService.GetSettings();
        }
    }
}