﻿namespace Ticketing.OrchardModule.Services
{
    using Orchard.Localization;
    using Orchard.UI.Navigation;

    public class AdminMenu : INavigationProvider
    {
        private const string AreaName = "Ticketing.OrchardModule";

        public string MenuName
        {
            get
            {
                return "admin";
            }
        }

        public Localizer T { get; set; }

        public void GetNavigation(NavigationBuilder builder)
        {
            builder.Add(this.T("Ticketing"), "4.1", this.BuildSubMenu);
        }

        private void BuildSubMenu(NavigationItemBuilder menu)
        {
            menu.Add(
                this.T("Manage Projects"),
                "4.1.1",
                item => item.Action("Index", "ProjectsAdmin", new { area = AreaName }).Permission(Permissions.ManageProjects));
            menu.Add(
                this.T("Create New Project"),
                "4.1.2",
                item => item.Action("CreateNew", "ProjectsAdmin", new { area = AreaName }).Permission(Permissions.ManageProjects));
        }
    }
}