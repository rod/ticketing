﻿namespace Ticketing.OrchardModule.Handlers
{
    using Orchard.ContentManagement;
    using Orchard.ContentManagement.Handlers;
    using Orchard.Data;
    using Orchard.Localization;

    using Ticketing.OrchardModule.Models;

    public class SettingsPartHandler : ContentHandler
    {
        public SettingsPartHandler(IRepository<SettingsPartRecord> repository)
        {
            this.Filters.Add(new ActivatingFilter<SettingsPart>("Site"));
            this.Filters.Add(StorageFilter.For(repository));
            this.T = NullLocalizer.Instance;
        }

        public Localizer T { get; set; }

        protected override void GetItemMetadata(GetContentItemMetadataContext context)
        {
            if (context.ContentItem.ContentType != "Site")
            {
                return;
            }

            base.GetItemMetadata(context);
            context.Metadata.EditorGroupInfo.Add(new GroupInfo(this.T("Ticketing")));
        }
    }
}