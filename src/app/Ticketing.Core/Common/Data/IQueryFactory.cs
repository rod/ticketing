﻿namespace Ticketing.Common.Data
{
    public interface IQueryFactory
    {
        T Create<T>();
    }
}
