﻿namespace Ticketing.Common.Data
{
    using System.Collections.Generic;

    public class PagedResult<T>
    {
        public int Page { get; set; }

        public int PageSize { get; set; }

        public int TotalCount { get; set; }

        public IEnumerable<T> PageOfItems { get; set; }
    }
}