﻿namespace Ticketing.Common.Data
{
    public interface IQuery<out TResult>
    {
        TResult Execute();
    }

    public interface IPagedQuery<T> : IQuery<PagedResult<T>>
    {
    }
}
