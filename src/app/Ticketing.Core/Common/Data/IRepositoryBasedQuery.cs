﻿namespace Ticketing.Common.Data
{
    /// <summary>
    /// Interface which determines queries which use repositories for selecting.
    /// </summary>
    public interface IRepositoryBasedQuery
    {
    }
}