﻿namespace Ticketing.Common.Data
{
    using System.Linq;

    public interface IRepository<T>
        where T : IAggregateRoot
    {
        IQueryable<T> Table { get; }

        void Add(T model);
    }
}
