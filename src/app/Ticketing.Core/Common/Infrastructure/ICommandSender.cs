﻿namespace Ticketing.Common.Infrastructure
{
    using Ticketing.Common;

    public interface ICommandSender
    {
        void Send<T>(T command) where T : ICommand;
    }
}
