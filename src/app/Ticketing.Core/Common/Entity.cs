namespace Ticketing.Common
{
    using System;

    public interface IIdentifiable
    {
        Guid Id { get; set; }
    }

    public interface IAggregateRoot : IIdentifiable
    {
    }

    public class Entity : IIdentifiable
    {
        public Guid Id { get; set; }
    }
}
