﻿namespace Ticketing.Common
{
    public interface ICommandHandler<in T> where T : ICommand
    {
        void HandleCommand(T command);
    }
}