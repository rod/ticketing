namespace Ticketing.TicketManagement.Domain
{
    public enum ProjectStatuses
    {
        /// <summary>
        /// First status of project.
        /// </summary>
        Started,

        /// <summary>
        /// Project successfully finished.
        /// </summary>
        Finished,

        /// <summary>
        /// Project manually terminated.
        /// </summary>
        Terminated,
    }
}