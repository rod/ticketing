namespace Ticketing.TicketManagement.Domain
{
    using System;

    using Ticketing.Common;
    using Ticketing.Utils;

    public class Project : Entity, IAggregateRoot
    {
        private readonly DateTime startingDate;

        private DateTime? finishedDate;

        public Project(string name, string owner)
        {
            if (string.IsNullOrEmpty(name))
            {
                throw new ArgumentNullException("name", "Name of the project cannot be null or empty");
            }

            if (string.IsNullOrEmpty(owner))
            {
                throw new ArgumentNullException("name", "Owner of the project cannot be null or empty");
            }

            this.Name = name;
            this.Owner = owner;
            this.Status = ProjectStatuses.Started;
            this.startingDate = DateTime.Now;
        }

        [Obsolete(ObsoleteReasons.ForNhOnly, true)]
        private Project()
        {
        }

        public string Name { get; private set; }

        public string Owner { get; private set; }

        public ProjectStatuses Status { get; private set; }

        public void Terminate()
        {
            this.Status = ProjectStatuses.Terminated;
            this.finishedDate = DateTime.Now;
        }

        public DateTime GetStatusDate()
        {
            if (this.Status == ProjectStatuses.Started)
            {
                return this.startingDate;
            }
            else
            {
                return this.finishedDate.Value;
            }
        }
    }
}
