﻿namespace Ticketing.TicketManagement.CommandHandlers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using Ticketing.Common;
    using Ticketing.Common.Data;
    using Ticketing.TicketManagement.Commands;
    using Ticketing.TicketManagement.Domain;

    public class ProjectService : ICommandHandler<CreateNewProjectCommand>, ICommandHandler<TerminateProjectsCommand>
    {
        private readonly IRepository<Project> projectRepository;

        public ProjectService(IRepository<Project> projectRepository)
        {
            this.projectRepository = projectRepository;
        }

        public void HandleCommand(CreateNewProjectCommand command)
        {
            var model = new Project(command.Name, command.OwnerLoginName);
            this.projectRepository.Add(model);
            command.CreatedProjectId = model.Id;
        }

        public void HandleCommand(TerminateProjectsCommand command)
        {
            command.ProjectIdsTerminated = new List<Guid>();

            foreach (var project in this.projectRepository.Table.Where(p => command.ProjectIdsToTerminate.Contains(p.Id)))
            {
                project.Terminate();
                command.ProjectIdsTerminated.Add(project.Id);
            }
        }
    }
}
