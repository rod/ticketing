﻿namespace Ticketing.TicketManagement.Queries
{
    using System.Linq;

    using Ticketing.Common.Data;
    using Ticketing.TicketManagement.Domain;
    using Ticketing.TicketManagement.Queries.ResultItems;

    public class ProjectAdminListQuery : IProjectAdminListQuery, IRepositoryBasedQuery
    {
        private IRepository<Project> projectRepository;

        public ProjectAdminListQuery(IRepository<Project> projectRepository)
        {
            this.projectRepository = projectRepository;
        }

        public ProjectStatuses? Status { get; set; }

        public PagedResult<ProjectAdminListItem> Execute()
        {
            var result = new PagedResult<ProjectAdminListItem>();
            var query = this.projectRepository.Table;
            if (this.Status != null)
            {
                query = query.Where(project => project.Status == this.Status.Value);
            }

            result.PageOfItems = from p in query.ToList()
                                 select new ProjectAdminListItem
                                 {
                                     ProjectId = p.Id,
                                     Name = p.Name,
                                     Owner = p.Owner,
                                     State = p.Status,
                                     StateDate = p.GetStatusDate(),
                                 };
            return result;
        }
    }
}
