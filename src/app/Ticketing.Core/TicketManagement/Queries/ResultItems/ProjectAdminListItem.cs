﻿namespace Ticketing.TicketManagement.Queries.ResultItems
{
    using System;

    using Ticketing.TicketManagement.Domain;

    public class ProjectAdminListItem
    {
        public Guid ProjectId { get; set; }

        public string Name { get; set; }

        public string Owner { get; set; }

        public ProjectStatuses State { get; set; }

        public DateTime StateDate { get; set; }
    }
}