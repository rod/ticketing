﻿namespace Ticketing.TicketManagement.Queries
{
    using Ticketing.Common.Data;
    using Ticketing.TicketManagement.Domain;
    using Ticketing.TicketManagement.Queries.ResultItems;

    public interface IProjectAdminListQuery : IPagedQuery<ProjectAdminListItem>
    {
        ProjectStatuses? Status { get; set; }
    }
}
