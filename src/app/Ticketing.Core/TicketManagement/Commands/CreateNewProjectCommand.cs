﻿namespace Ticketing.TicketManagement.Commands
{
    using System;
    using System.ComponentModel.DataAnnotations;

    using Ticketing.Common;

    public class CreateNewProjectCommand : ICommand
    {
        [Required(AllowEmptyStrings = false)]
        public string Name { get; set; }

        [Required(AllowEmptyStrings = false)]
        public string OwnerLoginName { get; set; }

        public Guid? CreatedProjectId { get; set; }
    }
}
