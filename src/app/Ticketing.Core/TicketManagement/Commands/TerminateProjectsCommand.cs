﻿namespace Ticketing.TicketManagement.Commands
{
    using System;
    using System.Collections.Generic;

    using Ticketing.Common;

    public class TerminateProjectsCommand : ICommand
    {
        public Guid[] ProjectIdsToTerminate { get; set; }

        public IList<Guid> ProjectIdsTerminated { get; set; }
    }
}