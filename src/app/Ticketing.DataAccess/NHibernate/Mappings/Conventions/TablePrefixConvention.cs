﻿namespace Ticketing.DataAccess.NHibernate.Mappings.Conventions
{
    using System;
    using System.Collections.Generic;

    using FluentNHibernate.Conventions;
    using FluentNHibernate.Conventions.AcceptanceCriteria;
    using FluentNHibernate.Conventions.Inspections;
    using FluentNHibernate.Conventions.Instances;

    public class TablePrefixConvention : IClassConvention, IClassConventionAcceptance
    {
        private string tablePrefix;

        private List<Type> typesToApply;

        public TablePrefixConvention(string tablePrefix)
        {
            this.tablePrefix = tablePrefix;
            this.typesToApply = new List<Type>(new TypeSource().GetTypes());
        }

        public void Apply(IClassInstance instance)
        {
            instance.Table(string.Concat(this.tablePrefix, instance.EntityType.Name));
        }

        public void Accept(IAcceptanceCriteria<IClassInspector> criteria)
        {
            criteria.Expect(inspector => this.typesToApply.Contains(inspector.EntityType));
        }
    }
}