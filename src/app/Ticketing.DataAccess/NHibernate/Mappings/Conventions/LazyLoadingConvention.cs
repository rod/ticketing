﻿namespace Ticketing.DataAccess.NHibernate.Mappings.Conventions
{
    using FluentNHibernate.Conventions;
    using FluentNHibernate.Conventions.AcceptanceCriteria;
    using FluentNHibernate.Conventions.Inspections;
    using FluentNHibernate.Conventions.Instances;

    using Ticketing.Common;

    public class LazyLoadingConvention : IClassConvention, IClassConventionAcceptance
    {
        public void Apply(IClassInstance instance)
        {
            instance.Not.LazyLoad();
        }

        public void Accept(IAcceptanceCriteria<IClassInspector> criteria)
        {
            criteria.Expect(inspector => typeof(Entity).IsAssignableFrom(inspector.EntityType));
        }
    }
}
