﻿namespace Ticketing.DataAccess.NHibernate.Mappings.Overrides
{
    using FluentNHibernate;
    using FluentNHibernate.Automapping;
    using FluentNHibernate.Automapping.Alterations;

    using Ticketing.TicketManagement.Domain;

    public class ProjectMappingOverride : IAutoMappingOverride<Project>
    {
        public void Override(AutoMapping<Project> map)
        {
            map.Map(Reveal.Member<Project>("startingDate"))
                .Column("StartingDate")
                .Access.CamelCaseField();
            map.Map(Reveal.Member<Project>("finishedDate"))
                .Column("FinishedDate")
                .Access.CamelCaseField();
        }
    }
}