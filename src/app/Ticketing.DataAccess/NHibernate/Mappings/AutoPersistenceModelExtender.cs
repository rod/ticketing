﻿namespace Ticketing.DataAccess.NHibernate.Mappings
{
    using FluentNHibernate.Automapping;

    using Ticketing.Common;
    using Ticketing.DataAccess.NHibernate.Mappings.Conventions;
    using Ticketing.DataAccess.NHibernate.Mappings.Overrides;

    public class AutoPersistenceModelExtender
    {
        private string tablePrefix;

        public AutoPersistenceModelExtender(string tablePrefix)
        {
            this.tablePrefix = tablePrefix;
        }

        public void Extend(AutoPersistenceModel persistenceModel)
        {
            var domainAssembly = typeof(IAggregateRoot).Assembly;
            persistenceModel
                .AddTypeSource(new TypeSource())
                .IgnoreBase<Entity>()
                .Conventions.Add(new TablePrefixConvention(this.tablePrefix))
                .UseOverridesFromAssemblyOf<ProjectMappingOverride>()
                .Conventions.AddFromAssemblyOf<LazyLoadingConvention>();
        }
    }
}
