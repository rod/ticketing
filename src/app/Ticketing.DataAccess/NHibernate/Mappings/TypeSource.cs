﻿namespace Ticketing.DataAccess.NHibernate.Mappings
{
    using System;
    using System.Collections.Generic;

    using FluentNHibernate;
    using FluentNHibernate.Diagnostics;

    using Ticketing.Common;
    using Ticketing.TicketManagement.Domain;

    public class TypeSource : ITypeSource
    {
        public IEnumerable<Type> GetTypes()
        {
            yield return typeof(Entity);
            yield return typeof(Project);
        }

        public void LogSource(IDiagnosticLogger logger)
        {
        }

        public string GetIdentifier()
        {
            return null;
        }
    }
}