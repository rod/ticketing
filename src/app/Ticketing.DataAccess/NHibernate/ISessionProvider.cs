﻿namespace Ticketing.DataAccess.NHibernate
{
    using global::NHibernate;

    public interface ISessionProvider
    {
        ISession GetSession();

        IStatelessSession GetStatelessSession();
    }
}