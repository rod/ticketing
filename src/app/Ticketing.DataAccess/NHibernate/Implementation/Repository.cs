﻿namespace Ticketing.DataAccess.NHibernate.Implementation
{
    using System.Linq;

    using global::NHibernate;

    using global::NHibernate.Linq;

    using Ticketing.Common;
    using Ticketing.Common.Data;
    using Ticketing.DataAccess.NHibernate;

    public class Repository<T> : IRepository<T>
        where T : IAggregateRoot
    {
        private readonly ISessionProvider sessionProvider;

        public Repository(ISessionProvider sessionProvider)
        {
            this.sessionProvider = sessionProvider;
        }

        public IQueryable<T> Table
        {
            get
            {
                return this.Session.Linq<T>();
            }
        }

        private ISession Session
            {
                get
                {
                    return this.sessionProvider.GetSession();
                }
            }

        public void Add(T model)
        {
            this.Session.Save(model);
            this.Session.Flush();
        }
    }
}
