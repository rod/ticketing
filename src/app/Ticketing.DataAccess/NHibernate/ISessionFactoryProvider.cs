﻿namespace Ticketing.DataAccess.NHibernate
{
    using global::NHibernate;

    public interface ISessionFactoryProvider
    {
        ISessionFactory GetFactory();
    }
}