﻿namespace Ticketing.DataAccess.InMemory.Implementation
{
    using System;
    using System.Collections.Concurrent;
    using System.Linq;

    using Ticketing.Common;
    using Ticketing.Common.Data;

    public class Repository<T> : IRepository<T>
        where T : IAggregateRoot
    {
        private readonly ConcurrentDictionary<Guid, T> store = new ConcurrentDictionary<Guid, T>();

        public IQueryable<T> Table
        {
            get
            {
                return this.store.Values.ToList().AsQueryable();
            }
        }

        public void Add(T model)
        {
            if (model.Id != Guid.Empty)
            {
                throw new ArgumentException("model is already persisted", "model");
            }

            model.Id = Guid.NewGuid();

            this.store.AddOrUpdate(
                model.Id,
                model,
                (id, existing) => { throw new Exception("Thore should be no update"); });
        }
    }
}
