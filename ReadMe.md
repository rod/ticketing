Preparing environment to development
====================================

1. Install ruby 1.9.3.
2. Install bundler: `gem install bundler`.
3. Install required gems" `bundle install` in project directory.
4. Run `rake prepare:development` with administrative preferences. Optionally
it orchard version can be passed as argument if such Orchard zip exists in libs
folder `rake prepare:development[1.6.1]`

Building project
----------------

Default rake task is to build project. Just write `rake` or `rake build:project`.

Creating packages
-----------------

To create packages, orchard development server must be already after initial
setup. `rake build:packages'

Run Orchard development server
------------------------------

`rake start:orchard` runs Orchard through IIS Express on port defined in
configuration files. Optionally own port can be passed `rake
start:orchard[32222]`

Build configuration files
-------------------------

Build parameters are defined in build configuration file
`build\configs\settings.yml`
